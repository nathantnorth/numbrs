//! # numbrs
//!
//! `numbrs` (pronounced "numbers") is a rust library meant to facilitate mathematical
//! operations similar to Numpy


#![warn(missing_docs)]
#![deny(missing_docs)]

pub use array::Array;
pub mod array;


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn simple_test() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
